#+TITLE: Section 16
#+SUBTITLE: GUI Automation
#+AUTHOR: Mohammed Asir Shahid
#+EMAIL: MohammedShahid@protonmail.com
#+DATE: 2021-08-06

* Controlling the Mouse from Python

The ultimate automation code tools are programs that control the keyboard and mouse. These programs can send virtual mouse clicks and keystrokes. We will use the third party pyautogui module for this.

#+begin_src bash
pip install pyautogui
#+end_src

#+RESULTS:
| Defaulting  | to      | user       | installation       | because | normal                                             | site-packages | is                             | not      | writeable |
| Requirement | already | satisfied: | pyautogui          | in      | /home/mohammeds/.local/lib/python3.9/site-packages | (0.9.53)      |                                |          |           |
| Requirement | already | satisfied: | pymsgbox           | in      | /home/mohammeds/.local/lib/python3.9/site-packages | (from         | pyautogui)                     | (1.0.9)  |           |
| Requirement | already | satisfied: | PyTweening>=1.0.1  | in      | /home/mohammeds/.local/lib/python3.9/site-packages | (from         | pyautogui)                     | (1.0.3)  |           |
| Requirement | already | satisfied: | pyscreeze>=0.1.21  | in      | /home/mohammeds/.local/lib/python3.9/site-packages | (from         | pyautogui)                     | (0.1.27) |           |
| Requirement | already | satisfied: | pygetwindow>=0.0.5 | in      | /home/mohammeds/.local/lib/python3.9/site-packages | (from         | pyautogui)                     | (0.0.9)  |           |
| Requirement | already | satisfied: | mouseinfo          | in      | /home/mohammeds/.local/lib/python3.9/site-packages | (from         | pyautogui)                     | (0.1.3)  |           |
| Requirement | already | satisfied: | python3-Xlib       | in      | /home/mohammeds/.local/lib/python3.9/site-packages | (from         | pyautogui)                     | (0.15)   |           |
| Requirement | already | satisfied: | pyrect             | in      | /home/mohammeds/.local/lib/python3.9/site-packages | (from         | pygetwindow>=0.0.5->pyautogui) | (0.1.4)  |           |
| Requirement | already | satisfied: | pyperclip          | in      | /home/mohammeds/.local/lib/python3.9/site-packages | (from         | mouseinfo->pyautogui)          | (1.8.2)  |           |

The mouse controlling functions work by XY coordinates that make up your display.

#+begin_src python :results output :exports both

import pyautogui

print(pyautogui.size())

width,height=pyautogui.size()

print(pyautogui.position())

pyautogui.moveTo(3200,1000,duration=1.5)

pyautogui.moveRel(300,-100,duration=1.5)

#+end_src

#+RESULTS:
: Size(width=7440, height=2560)
: Point(x=3134, y=1333)


#+begin_src python :results output :exports both

import pyautogui

pyautogui.click(3534,1500,duration=1)
pyautogui.middleClick(3534,1500,duration=1)


pyautogui.click()

#+end_src

#+RESULTS:

#+begin_src python :results output :exports both

import pyautogui

print(pyautogui.position())

pyautogui.moveTo(2776,645,duration=1)

pyautogui.dragRel(500,0,duration=1)
pyautogui.dragRel(-500,0,duration=1)
pyautogui.dragRel(500,0,duration=1)
pyautogui.dragRel(-500,0,duration=1)
pyautogui.dragRel(2500,0,duration=1)
pyautogui.dragRel(-2500,0,duration=1)

#+end_src

#+RESULTS:

PyAutoGui has a failsafe in case you lose control of the mouse. If the mouse goes to the coordinates 0,0, then PyAutoGui will raise an exception and then stop.

We can use the displayMousePosition() function in order to have an easier way to figure out the exact position of any given point. It gives the realtime coordinates and the RGB of the pixel. However, we need to run this from a terminal.

* Controlling the Keyboard from Python

Now we will cover the keyboard automation through pyautogui.

#+begin_src python :results output :exports both

import pyautogui

pyautogui.click(1657,1376); pyautogui.typewrite("i Hello World")

#+end_src

#+RESULTS:
: Point(x=1657, y=1376)

What can we do for keys that can't be represented by single string characters, for example the arrow keys, or the shift keys.

#+begin_src python :results output :exports both

import pyautogui

print(pyautogui.position())

#pyautogui.click(); pyautogui.typewrite(['i','a','b','left','right','X','y'])

print(pyautogui.KEYBOARD_KEYS)


pyautogui.typewrite(["space",",",'enter','space',','])

pyautogui.press('enter')

#+end_src

#+RESULTS:
: Point(x=2431, y=1634)
: ['\t', '\n', '\r', ' ', '!', '"', '#', '$', '%', '&', "'", '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '@', '[', '\\', ']', '^', '_', '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}', '~', 'accept', 'add', 'alt', 'altleft', 'altright', 'apps', 'backspace', 'browserback', 'browserfavorites', 'browserforward', 'browserhome', 'browserrefresh', 'browsersearch', 'browserstop', 'capslock', 'clear', 'convert', 'ctrl', 'ctrlleft', 'ctrlright', 'decimal', 'del', 'delete', 'divide', 'down', 'end', 'enter', 'esc', 'escape', 'execute', 'f1', 'f10', 'f11', 'f12', 'f13', 'f14', 'f15', 'f16', 'f17', 'f18', 'f19', 'f2', 'f20', 'f21', 'f22', 'f23', 'f24', 'f3', 'f4', 'f5', 'f6', 'f7', 'f8', 'f9', 'final', 'fn', 'hanguel', 'hangul', 'hanja', 'help', 'home', 'insert', 'junja', 'kana', 'kanji', 'launchapp1', 'launchapp2', 'launchmail', 'launchmediaselect', 'left', 'modechange', 'multiply', 'nexttrack', 'nonconvert', 'num0', 'num1', 'num2', 'num3', 'num4', 'num5', 'num6', 'num7', 'num8', 'num9', 'numlock', 'pagedown', 'pageup', 'pause', 'pgdn', 'pgup', 'playpause', 'prevtrack', 'print', 'printscreen', 'prntscrn', 'prtsc', 'prtscr', 'return', 'right', 'scrolllock', 'select', 'separator', 'shift', 'shiftleft', 'shiftright', 'sleep', 'space', 'stop', 'subtract', 'tab', 'up', 'volumedown', 'volumemute', 'volumeup', 'win', 'winleft', 'winright', 'yen', 'command', 'option', 'optionleft', 'optionright']

If we want to use a keyboard shortcut like control+c, then we can use the hotkey function.
