
# Table of Contents

1.  [Sending Emails](#org0ff9af6)
    1.  [SMPT - Simple Mail Transfer Protocol](#orgad64f05)
2.  [Checking Your Email Inbox](#orgcdde008)
    1.  [IMAP - Internet Message Access Protocol](#org31825c2)



<a id="org0ff9af6"></a>

# Sending Emails

This can be a great way to send notifications. For example, if you are doing web scraping and your program finds something of note, perhaps a sale on an item, then it can send you an email notifying you. It can also help you email a bunch of people based on some criteria.


<a id="orgad64f05"></a>

## SMPT - Simple Mail Transfer Protocol

This is the protocol that sending email uses. Python has a module called smptlib which implements this protocol.

    
    import smtplib
    
    conn=smtplib.SMTP("smtp.gmail.com", 587)
    print(type(conn))
    
    conn.ehlo()
    print(conn.ehlo())
    
    conn.starttls()
    
    conn.login(send_email,password)
    
    conn.sendmail(send_email, recieve_email,"Subject: Test SMPT Email.\n\nDear Asir,\nSo long, and thanks for all the fish.\n\n-Asir")
    
    conn.quit()


<a id="orgcdde008"></a>

# Checking Your Email Inbox

We learned how to send emails using SMTP. For checking our received emails, we have a different protocol.


<a id="org31825c2"></a>

## IMAP - Internet Message Access Protocol

This species how to communicate with your email service provider in order to see your received mail. IMAP is pretty complicated as Email protocols are nearly as old as the internet. Python comes with an impablib module, however we will instead use two third party modules.

    pip install imapclient
    pip install pyzmail36

    
    import imapclient,pyzmail
    from datetime import date
    
    conn=imapclient.IMAPClient("imap.gmail.com", ssl=True)
    
    conn.login(email,password)
    
    
    conn.select_folder("INBOX", readonly=True)
    
    UID=conn.search(["FROM", recieved_from])
    UIDs=conn.search(["SINCE" ,date(2021,8,1)])
    
    raw_message=conn.fetch([62266], ["BODY[]", "FLAGS"])
    
    message=pyzmail.PyzMessage.factory(raw_message[62266][b"BODY[]"])
    
    subject=message.get_subject()
    
    print(subject)
    
    sender=message.get_addresses("from")
    reciever=message.get_addresses("to")
    
    print(sender, reciever)
    
    print(message.text_part)
    print(message.html_part)
    
    print(message.text_part.get_payload().decode("UTF-8"))
    
    #conn.logout
    
    print(conn.list_folders())

Above, we used the search method. When searching, IMAP has a particular syntax that you search with. We can use &ldquo;ALL&rdquo;, &ldquo;BEFORE date&rdquo;, &ldquo;ON date&rdquo;, &ldquo;SINCE date&rdquo;, &ldquo;SUBJECT string&rdquo;, &ldquo;BODY string&rdquo;, &ldquo;TEXT string&rdquo;, &ldquo;FROM string&rdquo;, &ldquo;TO string&rdquo;, &ldquo;CC string&rdquo;, &ldquo;SEEN&rdquo;, &ldquo;UNSEEN&rdquo;, &ldquo;ANSWERED&rdquo;, &ldquo;DRAFT&rdquo;, etc.

Above, we also set the inbox to readonly. If we want to be able to delete Emails, we need to set readonly to False. Then we can use the delete<sub>messages</sub> function and pass the UIDs that we want to delete.

